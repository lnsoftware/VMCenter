package com.hexun.vmcenter.client;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.hexun.vmcenter.anno.Frequency;
import com.hexun.vmcenter.client.wrap.Wrapper;
import com.hexun.vmcenter.util.Rate;

@Path(value = "vmcenter")
public interface VMCenterService {
	
	/**
	 * 查询用户流水
	 * 
	 * @param userId 用户ID, 必填
	 * @param pageSize 每页记录数, 非必填, 默认20
	 * @param pageNo 当前页数, 非必填, 默认1
	 */
	@Frequency(limit = 5, rate = Rate.SECOND)
	@GET
	@Path(value = "user-detail")
	@Produces({MediaType.APPLICATION_JSON})
	public Wrapper<?> userDetail(@QueryParam(value = "pageSize") String pageSize,
			@QueryParam(value = "pageNo") String pageNo,
			@Context HttpServletRequest request) throws Exception;
}
