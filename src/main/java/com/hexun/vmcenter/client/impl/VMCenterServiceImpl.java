package com.hexun.vmcenter.client.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.hexun.hwcommon.service.UserAuth;
import com.hexun.hwcommon.util.LogoSizeStyles;
import com.hexun.vmcenter.client.VMCenterService;
import com.hexun.vmcenter.client.wrap.WrapMapper;
import com.hexun.vmcenter.client.wrap.Wrapper;
import com.hexun.vmcenter.domain.CoinItem;
import com.hexun.vmcenter.domain.UserDetail;
import com.hexun.vmcenter.domain.UserInfo;
import com.hexun.vmcenter.util.HttpUtil;

/**
 * 虚拟币对外接口
 * 
 * @author hexun
 */
@Component
public class VMCenterServiceImpl implements VMCenterService {

	/**
	 * 获取用户明细
	 */
	public Wrapper<?> userDetail(String pageSize, String pageNo, HttpServletRequest request) throws Exception {
		String userId = "25067179";
//		CommonLoginInfo cli = UserAuth.GetUserInfoByRequest(request);
//		if (cli != null && cli.getIslogin().equals("True")) {
//			userId = cli.getUserid();
//		} else {
//			return WrapMapper.wrap(Wrapper.ERROR_CODE, "用户ID错误");
//		}
		
		UserDetail ud = new UserDetail();
		
		// 1.查询账户余额
		
		/** 账户余额URL */
		String HECOIN_USER_ACCOUNT_URL = "http://vm.intcoop.hexun.com:8060/getUserCoin?userId=$USER_ID$";

		HECOIN_USER_ACCOUNT_URL = HECOIN_USER_ACCOUNT_URL.replace("$USER_ID$", userId);
		String accountResultStr = HttpUtil.sendGet(HECOIN_USER_ACCOUNT_URL);
		if(!StringUtils.isBlank(accountResultStr)){
			JSONObject jo = JSONObject.fromObject(accountResultStr);
			if(jo.getInt("statusCode") == 200){
				ud.setAccount(jo.getDouble("data"));
			} else {
				return WrapMapper.wrap(Wrapper.ERROR_CODE,jo.getString("moreInfo"));
			}
		}
		
		// 2. 查询账户流水
		
		/** 账户流水明细URL */
		String HECOIN_ITEMS_URL = "http://vm.intcoop.hexun.com:8060/getAllCoinDetailById?userId=$USER_ID$&pageSize=$PAGE_SIZE$&pageNo=$PAGE_NO$";
		
		HECOIN_ITEMS_URL = HECOIN_ITEMS_URL.replace("$USER_ID$", userId);
		// 每页记录数
		if(!StringUtils.isBlank(pageSize)){
			HECOIN_ITEMS_URL = HECOIN_ITEMS_URL.replace("$PAGE_SIZE$", pageSize);
		} else {
			HECOIN_ITEMS_URL = HECOIN_ITEMS_URL.replace("$PAGE_SIZE$", "20");
		}
		// 当前页
		if(!StringUtils.isBlank(pageNo)){
			HECOIN_ITEMS_URL = HECOIN_ITEMS_URL.replace("$PAGE_NO$", pageNo);
		} else {
			HECOIN_ITEMS_URL = HECOIN_ITEMS_URL.replace("$PAGE_NO$", "1");
		}
		// 发送请求
		String itemsResultStr = HttpUtil.sendGet(HECOIN_ITEMS_URL);
		if(!StringUtils.isBlank(itemsResultStr)){
			JSONObject jo = JSONObject.fromObject(itemsResultStr);
			if(jo.getInt("statusCode") == 200){
				JSONArray ja = jo.getJSONObject("data").getJSONArray("list");
				if(ja != null && ja.size() > 0){
					List<CoinItem> list = new ArrayList<CoinItem>();
					for(int i=0; i<ja.size(); i++){
						JSONObject itemJO = ja.getJSONObject(i);
						CoinItem ci = new CoinItem();
						ci.setInCome(itemJO.get("inCome") != null ? new BigDecimal(itemJO.getDouble("inCome")) : BigDecimal.ZERO);
						ci.setNowCome(itemJO.get("nowCome") != null ? new BigDecimal(itemJO.getDouble("nowCome")) : BigDecimal.ZERO);
						ci.setOutCome(itemJO.get("outCome") != null ? new BigDecimal(itemJO.getDouble("outCome")) : BigDecimal.ZERO);
						ci.setPlatCode(itemJO.getString("platCode"));
						ci.setPlatForm(itemJO.getString("platForm"));
						ci.setRecordTime(itemJO.get("recordTime") != null ? StringToDate(itemJO.getString("recordTime"), "yyyy-MM-dd HH:mm:ss") : null);
						ci.setRemark(itemJO.getString("remark"));
						ci.setThirdId(itemJO.getString("thirdId"));
						ci.setTypeKey(itemJO.getString("typeKey"));
						ci.setUserId(itemJO.get("userId") != null ? itemJO.getLong("userId") : 0);
						ci.setUserName(itemJO.getString("userName"));
						list.add(ci);
					}
					ud.setItems(list);
				}
			} else {
				return WrapMapper.wrap(Wrapper.ERROR_CODE,jo.getString("moreInfo"));
			}
		}
		
		// 3. 老师信息
		UserInfo ui = new UserInfo();
		ui.setUserId(Long.parseLong(userId));
		ui.setNickName(UserAuth.GetNickNameByUserID(Long.parseLong(userId)));
		ui.setHeaderImage(com.hexun.hwcommon.service.UserInfo.GetLogoUrl(Long.parseLong(userId), LogoSizeStyles.s40));
		ud.setUser(ui);
		
		return WrapMapper.wrap(Wrapper.SUCCESS_CODE, Wrapper.SUCCESS_MESSAGE, ud);
	}
	
	/** 
     * 将日期字符串转化为日期
     * 
     * @param date 日期字符串 
     * @param parttern 日期格式 
     * @return 日期 
     */  
    public static Date StringToDate(String date, String parttern) {  
        Date myDate = null;  
        if (date != null) {  
            try {  
                myDate = new SimpleDateFormat(parttern).parse(date);  
            } catch (Exception e) { 
            	e.printStackTrace();
            }  
        }  
        return myDate;  
    }  
    
}
