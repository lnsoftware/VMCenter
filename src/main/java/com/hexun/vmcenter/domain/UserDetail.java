package com.hexun.vmcenter.domain;

import java.util.List;

public class UserDetail {

	/** 账户余额 */
	private Double account;
	
	/** 用户信息 */
	private UserInfo user;
	
	/** 明细 */
	private List<CoinItem> items;

	public Double getAccount() {
		return account;
	}

	public void setAccount(Double account) {
		this.account = account;
	}

	public UserInfo getUser() {
		return user;
	}

	public void setUser(UserInfo user) {
		this.user = user;
	}

	public List<CoinItem> getItems() {
		return items;
	}

	public void setItems(List<CoinItem> items) {
		this.items = items;
	}
	
}
