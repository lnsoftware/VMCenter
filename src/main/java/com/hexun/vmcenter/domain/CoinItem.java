package com.hexun.vmcenter.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 虚拟币流水实体
 * 
 * @author hexun
 */
public class CoinItem implements Serializable {

	private static final long serialVersionUID = -2516812293902827751L;

	/** 用户ID */
	private Long userId;
	
	/** 用户名称 */
	private String userName;
	
	/** 类型ID */
	private String typeKey;
	
	/**平台编号**/
	private String platCode;
	
	/** 平台名称 */
	private String platForm;
	
	/** 收入 */
	private BigDecimal inCome;
	
	/** 支出 */
	private BigDecimal outCome;
	
	/** 当前可用虚拟币 **/
	private BigDecimal nowCome;
	
	/** 记录日期 */
	private Date recordTime;
	
	/** 备注 */
	private String remark;
	
	/** 第三方平台交易ID **/
	private String thirdId;

	public String getThirdId() {
		return thirdId;
	}

	public void setThirdId(String thirdId) {
		this.thirdId = thirdId;
	}

	public String getPlatForm() {
		return platForm;
	}

	public void setPlatForm(String platForm) {
		this.platForm = platForm;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTypeKey() {
		return typeKey;
	}

	public void setTypeKey(String typeKey) {
		this.typeKey = typeKey;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public BigDecimal getInCome() {
		return inCome;
	}

	public void setInCome(BigDecimal inCome) {
		this.inCome = inCome;
	}

	public BigDecimal getOutCome() {
		return outCome;
	}

	public void setOutCome(BigDecimal outCome) {
		this.outCome = outCome;
	}

	public BigDecimal getNowCome() {
		return nowCome;
	}

	public void setNowCome(BigDecimal nowCome) {
		this.nowCome = nowCome;
	}

	public Date getRecordTime() {
		return recordTime;
	}

	public void setRecordTime(Date recordTime) {
		this.recordTime = recordTime;
	}

	public String getPlatCode() {
		return platCode;
	}

	public void setPlatCode(String platCode) {
		this.platCode = platCode;
	}
	
}
