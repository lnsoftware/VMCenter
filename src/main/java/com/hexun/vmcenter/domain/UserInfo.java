package com.hexun.vmcenter.domain;

public class UserInfo {

	/** 用户ID */
	private Long userId;
	
	/** 昵称 */
	private String nickName;
	
	/** 头像 */
	private String headerImage;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getHeaderImage() {
		return headerImage;
	}

	public void setHeaderImage(String headerImage) {
		this.headerImage = headerImage;
	}
	
}
