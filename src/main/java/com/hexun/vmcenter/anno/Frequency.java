package com.hexun.vmcenter.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import com.hexun.vmcenter.util.Rate;

/**
 * 访问次数限制
 * 
 * @author Sammax
 * 
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Order(Ordered.HIGHEST_PRECEDENCE)
public @interface Frequency {
	
	/**
	 * 毫秒
	 */
	Rate rate();

	/**
	 * 次数
	 */
	int limit();

}
