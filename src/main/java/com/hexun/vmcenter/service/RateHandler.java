package com.hexun.vmcenter.service;


import com.hexun.vmcenter.util.Rate;

/**
 * Created by YanYang on 2016/6/24.
 */
public interface RateHandler {

    void insert(String requestMethod, long timestamp, boolean increment, Rate rate) throws Exception;

    boolean isLimit(String requestMethod, long currentTime, int number, Rate rate) throws Exception;
}
